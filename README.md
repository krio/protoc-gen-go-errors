# protoc-gen-go-errors

#### 介绍
protoc-gen-go-errors  
借鉴b站的开源框架错误处理机制：go-kratos ，感谢大b站！！！（其实就是抄，哈哈！！！）  
框架手册地址： https://go-kratos.dev/docs/getting-started/start    

#### 软件架构
```
├── README.md
├── errors
│   ├── errors.go
│   ├── errors.pb.go
│   └── errors.proto
├── errors.go
├── go.mod
├── go.sum
├── main.go
├── status
│   └── status.go
└── template.go
```


#### 安装教程

1. go get -u gitee.com/krio/protoc-gen-go-errors
2. 通过 proto生成对应的代码：xxx.proto =>(生成)=> xxx.pb.go 、 xxx_errors.pb.go  (建议将命令集成到 Makefile)
``` Makefile
ERROR_DIR     = ./error/demo/
ERROR_PROTO_FILES = $(wildcard $(ERROR_DIR)*.proto)

error:
	protoc --proto_path=. \
     --proto_path=./error/third_party \
     --go_out=paths=source_relative:. \
     --go-errors_out=paths=source_relative:. \
     $(ERROR_PROTO_FILES)
``` 

==注意事项:==
- 当枚举组没有配置缺省错误码时, 当前枚举组的没有配置错误码的枚举值会被忽略
- 当整个枚举组都没配置错误码时，当前枚举组会被忽略
- 错误码的取值范围应该在 0 < code <= 600 之间, 超出范围将抛出异常
- example/third_party 一定要有并根据里面说明调整

#### 使用说明
```golang
# 响应错误：

// 通过 errors.New() 响应错误
errors.New(500, "USER_NAME_EMPTY", "user name is empty")

// 通过 proto 生成的代码响应错误，并且包名应替换为自己生成代码后的 package name
api.ErrorUserNotFound("user %s not found", "kratos")

// 传递metadata
err := errors.New(500, "USER_NAME_EMPTY", "user name is empty")
err = err.WithMetadata(map[string]string{
    "foo": "bar",
})

#错误断言：
err := wrong()

// 通过 errors.Is() 断言
if errors.Is(err,errors.BadRequest("USER_NAME_EMPTY","")) {
    // do something
}

// 通过判断 *Error.Reason 和 *Error.Code
e := errors.FromError(err)
if  e.Reason == "USER_NAME_EMPTY" && e.Code == 500 {
    // do something
}

// 通过 proto 生成的代码断言错误，并且包名应替换为自己生成代码后的 package name
if api.IsUserNotFound(err) {
        // do something
})
```

