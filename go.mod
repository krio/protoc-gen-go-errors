module gitee.com/krio/protoc-gen-go-errors

go 1.13

require (
	github.com/golang/protobuf v1.5.2
	github.com/google/go-cmp v0.5.6 // indirect
	google.golang.org/genproto v0.0.0-20210629200056-84d6f6074151
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.27.1
)
